using System;
using System.Collections.Generic;

namespace NumbersToWords.Models
{
  public static class NumberToWordConverter
  {
    private static Dictionary<int, string> _onesConversion = new Dictionary<int, string> {
      {0, "zero"},
      {1, "one"},
      {2, "two"},
      {3, "three"},
      {4, "four"},
      {5, "five"},
      {6, "six"},
      {7, "seven"},
      {8, "eight"},
      {9, "nine"}
    };
    private static Dictionary<int, string> _teensConversion = new Dictionary<int, string> {
      {10, "ten"},
      {11, "eleven"},
      {12, "twelve"},
      {13, "thirteen"},
      {14, "fourteen"},
      {15, "fifteen"},
      {16, "sixteen"},
      {17, "seventeen"},
      {18, "eighteen"},
      {19, "nineteen"}
    };
    private static Dictionary<int, string> _tensPrefixConversion = new Dictionary<int, string> {
      {20, "twenty"},
      {30, "thirty"},
      {40, "fourty"},
      {50, "fifty"},
      {60, "sixty"},
      {70, "seventy"},
      {80, "eighty"},
      {90, "ninety"}
    };
    private static Dictionary<int, string> _magnitudePrefixConversion = new Dictionary<int, string> {
      {1000, "thousand"},
      {1000000, "million"}
    };

    public static string Convert(int number)
    {
      if (number < 0)
      {
        throw new Exception("Out of conversion range");
      }
      string output = "";
      if (number == 0)
      {
        output = _onesConversion[0];
      }
      // TODO: millions logic
      int millions = number / 1000000;
      int thousands = number / 1000;
      int hundreds = number % 1000;

      if (thousands != 0)
      {
        output += ConvertHundredsPart(thousands);
        output += " thousand";
        if (hundreds != 0)
        {
          output += " ";
        }
      }
      if (hundreds != 0)
      {
        output += ConvertHundredsPart(hundreds);
      }
      return output;
    }

    private static string ConvertTensPart(int number)
    {
      if (number < 0 || number > 99)
      {
        throw new Exception("Out of conversion range");
      }
      string output = "";
      if (number < 10)
      {
        output += ConvertOnesPlace(number);
      }
      else if (number < 20)
      {
        output += ConvertTeens(number);
      }
      else if (number < 100)
      {
        int tens = (number / 10) * 10;
        int ones = number % 10;
        output += ConvertTensPrefix(tens);
        if (ones != 0)
        {
          output += " ";
          output += ConvertOnesPlace(ones);
        }
      }
      return output;
    }
    private static string ConvertHundredsPart(int number)
    {
      if (number < 0 || number > 999)
      {
        throw new Exception("Out of conversion range");
      }
      string output = "";
      int hundreds = number / 100;
      int tensAndOnes = number % 100;
      if (hundreds != 0)
      {
        output += ConvertOnesPlace(hundreds);
        output += " hundred";
        if (tensAndOnes != 0)
        {
          output += " ";
        }
      }
      if (tensAndOnes != 0)
      {
        output += ConvertTensPart(tensAndOnes);
      }
      return output;
    }

    private static string ConvertOnesPlace(int number)
    {
      if (_onesConversion.ContainsKey(number))
      {
        return _onesConversion[number];
      }
      throw new Exception("Out of conversion range");
    }
    private static string ConvertTeens(int number)
    {
      if (_teensConversion.ContainsKey(number))
      {
        return _teensConversion[number];
      }
      throw new Exception("Out of conversion range");
    }
    private static string ConvertTensPrefix(int number)
    {
      if (_tensPrefixConversion.ContainsKey(number))
      {
        return _tensPrefixConversion[number];
      }
      throw new Exception("Out of conversion range");
    }
  }
}
